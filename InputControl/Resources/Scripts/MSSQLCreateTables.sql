﻿if not exists (select * from sysobjects where name='Accounts' and xtype='U')
create table dbo.Accounts
(
  Login    varchar(255) not null
    primary key,
  Password varchar(255) not null
);

if not exists (select * from sysobjects where name='Countries' and xtype='U')
create table dbo.Countries
(
  Id   int identity
    primary key,
  Name varchar(255) not null
);

if not exists (select * from sysobjects where name='Educations' and xtype='U')
create table dbo.Educations
(
  Id    int identity
    primary key,
  Name  varchar(255) not null,
  Level varchar(255) not null
);

if not exists (select * from sysobjects where name='Organizations' and xtype='U')
create table dbo.Organizations
(
  Id        int identity
    primary key,
  Name      varchar(255) not null,
  CountryId int          not null
    constraint Organizations_Countries_Id_fk
    references Countries (Id)
);

if not exists (select * from sysobjects where name='WorkPositions' and xtype='U')
create table dbo.WorkPositions
(
  Id   int identity
    primary key,
  Name varchar(255) not null,
  Pay  int          not null
);

if not exists (select * from sysobjects where name='Employees' and xtype='U')
create table dbo.Employees
(
  Id             int identity
    primary key,
  Login          varchar(255) not null,
  FirstName      varchar(255) not null,
  LastName       varchar(255) not null,
  CountryId      int          not null
    constraint Employees_Countries_Id_fk
    references Countries (Id),
  WorkPositionId int
    constraint Employees_WorkPositions_Id_fk
    references WorkPositions (Id),
  OrganizationId int
    constraint Employees_Organizations_Id_fk
    references Organizations (Id),
  EducationId    int
    constraint Employees_Educations_Id_fk
    references Educations
);