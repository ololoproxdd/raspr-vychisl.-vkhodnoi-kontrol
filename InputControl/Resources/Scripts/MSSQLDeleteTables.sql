﻿if exists (select * from sysobjects where name='Employees' and xtype='U')
	drop table dbo.Employees;

if exists (select * from sysobjects where name='WorkPositions' and xtype='U')
	drop table dbo.WorkPositions;

if exists (select * from sysobjects where name='Organizations' and xtype='U')
	drop table dbo.Organizations;

if exists (select * from sysobjects where name='Educations' and xtype='U')
	drop table dbo.Educations;

if exists (select * from sysobjects where name='Countries' and xtype='U')
	drop table dbo.Countries;
	
if exists (select * from sysobjects where name='Accounts' and xtype='U')
	drop table dbo.Accounts;