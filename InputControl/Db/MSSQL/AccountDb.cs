﻿using InputControl.Models.MSSQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputControl.Context.MSSQL
{
    public static class AccountDb
    {
        public static string Insert(string login, string password)
        {
            return Insert(new Account(login, password));
        }

        public static string Insert(Account account)
        {
            using (var db = new MSSQLContext())
            {
                var dbAccount = db.Accounts.Where(w => w.Login.Equals(account.Login)
                    && w.Password.Equals(account.Password))?.FirstOrDefault() ?? null;

                if (dbAccount == null)
                {
                    var result = db.Accounts.Add(account);
                    db.SaveChanges();
                    return result.Login;
                }

                return dbAccount.Login;
            }
        }
    }
}
