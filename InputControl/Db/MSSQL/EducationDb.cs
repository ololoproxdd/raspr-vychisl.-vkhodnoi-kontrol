﻿using InputControl.Models.MSSQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputControl.Context.MSSQL
{
    public static class EducationDb
    {
        public static int Insert(string name, string level)
        {
            return Insert(new Education(name, level));
        }

        public static int Insert(Education education)
        {
            using (var db = new MSSQLContext())
            {
                var dbEducation = db.Educations.Where(w => w.Name.Equals(education.Name)
                    && w.Level.Equals(education.Level))?.FirstOrDefault() ?? null;

                if (dbEducation == null)
                {
                    var result = db.Educations.Add(education);
                    db.SaveChanges();
                    return result.Id;
                }

                return dbEducation.Id;
            }
        }
    }
}
