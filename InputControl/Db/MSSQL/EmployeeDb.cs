﻿using InputControl.Models.MSSQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputControl.Context.MSSQL
{
    public static class EmployeeDb
    {
        public static int Insert(Models.SQLite.Employee emp)
        {
            AccountDb.Insert(emp.Login, emp.Password);
            var countryId = CountryDb.Insert(emp.Country);
            var eduId = EducationDb.Insert(emp.Education, emp.EducationLvl);
            var orgId = OrganizationDb.Insert(emp.Organization, emp.OrganizationCountry);
            var wpId = WorkPositionDb.Insert(emp.WorkPosition, emp.Pay);

            var mssqlEmp = new Employee(emp.Login, emp.FirstName, emp.LastName, countryId, wpId, orgId, eduId);
            return Insert(mssqlEmp);
        }

        public static int Insert(Employee employee)
        {
            using (var db = new MSSQLContext())
            {
                var dbEmployee = db.Employees.Where(w => w.Login.Equals(employee.Login)
                    && w.FirstName.Equals(employee.FirstName)
                    && w.LastName.Equals(employee.LastName)
                    && w.CountryId.Equals(employee.CountryId)
                    && w.WorkPositionId.Equals(employee.WorkPositionId)
                    && w.OrganizationId.Equals(employee.OrganizationId)
                    && w.EducationId.Equals(employee.EducationId))?.FirstOrDefault() ?? null;

                if (dbEmployee == null)
                {
                    var result = db.Employees.Add(employee);
                    db.SaveChanges();
                    return result.Id;
                }

                return dbEmployee.Id;
            }
        }
    }
}
