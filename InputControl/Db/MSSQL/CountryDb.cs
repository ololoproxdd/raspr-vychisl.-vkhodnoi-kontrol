﻿using InputControl.Models.MSSQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputControl.Context.MSSQL
{
    public static class CountryDb
    {
        public static int Insert(string name)
        {
            return Insert(new Country(name));
        }

        public static int Insert(Country country)
        {
            using (var db = new MSSQLContext())
            {
                var dbCountry = db.Countries.Where(w => w.Name.Equals(country.Name))?.FirstOrDefault() ?? null;
                if (dbCountry == null)
                {
                    var result = db.Countries.Add(country);
                    db.SaveChanges();
                    return result.Id;
                }

                return dbCountry.Id;
            }
        }
    }
}
