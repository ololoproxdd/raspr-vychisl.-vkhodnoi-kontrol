﻿using InputControl.Models.MSSQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputControl.Context.MSSQL
{
    public static class WorkPositionDb
    {
        public static int Insert(string name, int pay)
        {
            return Insert(new WorkPosition(name, pay));
        }

        public static int Insert(WorkPosition workPosition)
        {
            using (var db = new MSSQLContext())
            {
                var dbWorkPosition = db.WorkPositions.Where(w => w.Name.Equals(workPosition.Name)
                    && w.Pay.Equals(workPosition.Pay))?.FirstOrDefault() ?? null;

                if (dbWorkPosition == null)
                {
                    var result = db.WorkPositions.Add(workPosition);
                    db.SaveChanges();
                    return result.Id;
                }

                return dbWorkPosition.Id;
            }
        }
    }
}
