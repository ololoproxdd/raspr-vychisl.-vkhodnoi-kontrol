﻿using InputControl.Models.MSSQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputControl.Context.MSSQL
{
    public static class OrganizationDb
    {
        public static int Insert(string name, string country)
        {
            using (var db = new MSSQLContext())
            {
                int? countryId = db.Countries.Where(w => w.Name.Equals(country))?.FirstOrDefault()?.Id ?? null;
                if (countryId == null)
                    countryId = CountryDb.Insert(country);

                return Insert(new Organization(name, countryId.Value));
            }
        }

        public static int Insert(Organization organization)
        {
            using (var db = new MSSQLContext())
            {
                var dbOrganization = db.Organizations.Where(w => w.Name.Equals(organization.Name)
                    && w.CountryId.Equals(organization.CountryId))?.FirstOrDefault() ?? null;

                if (dbOrganization == null)
                {
                    var result = db.Organizations.Add(organization);
                    db.SaveChanges();
                    return result.Id;
                }

                return dbOrganization.Id;
            }
        }
    }
}
