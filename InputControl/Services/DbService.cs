﻿using InputControl.Context.MSSQL;
using InputControl.Context.SQLite;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputControl.Services
{
    public static class DbService
    {
        public static void DeleteMSSQLDb()
        {
            using (var db = new MSSQLContext())
            {
                using (StreamReader sr = new StreamReader("./Resources/Scripts/MSSQLDeleteTables.sql"))
                {
                    db.Database.ExecuteSqlCommand(sr.ReadToEnd());
                }
            }
        }

        public static void CreateMSSQLDb()
        {
            using (var db = new MSSQLContext())
            {
                using (StreamReader sr = new StreamReader("./Resources/Scripts/MSSQLCreateTables.sql"))
                {
                    db.Database.ExecuteSqlCommand(sr.ReadToEnd());
                }
            }
        }

        public static void Normalization()
        {
            using (var db = new SQLiteContext())
            {
                db.Employees.Load();
                foreach (var emp in db.Employees)
                {
                    EmployeeDb.Insert(emp);
                }
            }
        }

        public static void ExportToExcel()
        {

        }
    }
}
