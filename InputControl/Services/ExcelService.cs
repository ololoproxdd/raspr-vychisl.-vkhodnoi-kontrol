﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InputControl.Context.MSSQL;
using OfficeOpenXml;

namespace InputControl.Services
{
    public static class ExcelService
    {

        public static void ExportFromMSSQL()
        {
            using (var p = new ExcelPackage())
            {
                using (var db = new MSSQLContext())
                {
                    ExportAccounts(p, db);
                    ExportCountries(p, db);
                    ExportEducations(p, db);
                    ExportEmployees(p, db);
                    ExportOrganizations(p, db);
                    ExportWorkPositions(p, db); 
                }
                p.SaveAs(new FileInfo(@"../../Resources/Employees.xlsx"));
            }
        }

        private static void ExportAccounts(ExcelPackage p, MSSQLContext db)
        {
            var ws = p.Workbook.Worksheets.Add("Accounts");
            ws.Cells[1, 1].Value = "Login";
            ws.Cells[1, 2].Value = "Password";
            var accounts = db.Accounts.ToList();
            for (int i = 0; i < accounts.Count(); i++)
            {
                var acc = accounts[i];
                ws.Cells[i + 2, 1].Value = acc.Login;
                ws.Cells[i + 2, 2].Value = acc.Password;
            }
        }

        private static void ExportCountries(ExcelPackage p, MSSQLContext db)
        {
            var ws = p.Workbook.Worksheets.Add("Countries");
            ws.Cells[1, 1].Value = "Id";
            ws.Cells[1, 2].Value = "Name";
            var countries = db.Countries.ToList();
            for (int i = 0; i < countries.Count(); i++)
            {
                var country = countries[i];
                ws.Cells[i + 2, 1].Value = country.Id;
                ws.Cells[i + 2, 2].Value = country.Name;
            }
        }

        private static void ExportEducations(ExcelPackage p, MSSQLContext db)
        {
            var ws = p.Workbook.Worksheets.Add("Educations");
            ws.Cells[1, 1].Value = "Id";
            ws.Cells[1, 2].Value = "Name";
            ws.Cells[1, 3].Value = "Level";
            var educations = db.Educations.ToList();
            for (int i = 0; i < educations.Count(); i++)
            {
                var edu = educations[i];
                ws.Cells[i + 2, 1].Value = edu.Id;
                ws.Cells[i + 2, 2].Value = edu.Name;
                ws.Cells[i + 2, 3].Value = edu.Level;
            }
        }

        private static void ExportEmployees(ExcelPackage p, MSSQLContext db)
        {
            var ws = p.Workbook.Worksheets.Add("Employees");
            ws.Cells[1, 1].Value = "Id";
            ws.Cells[1, 2].Value = "Login";
            ws.Cells[1, 3].Value = "FirstName";
            ws.Cells[1, 4].Value = "LastName";
            ws.Cells[1, 5].Value = "CountryId";
            ws.Cells[1, 6].Value = "WorkPositionId";
            ws.Cells[1, 7].Value = "OrganizationId";
            ws.Cells[1, 8].Value = "EducationId";
            var employees = db.Employees.ToList();
            for (int i = 0; i < employees.Count(); i++)
            {
                var emp = employees[i];
                ws.Cells[i + 2, 1].Value = emp.Id;
                ws.Cells[i + 2, 2].Value = emp.Login;
                ws.Cells[i + 2, 3].Value = emp.FirstName;
                ws.Cells[i + 2, 4].Value = emp.LastName;
                ws.Cells[i + 2, 5].Value = emp.CountryId;
                ws.Cells[i + 2, 6].Value = emp.WorkPositionId;
                ws.Cells[i + 2, 7].Value = emp.OrganizationId;
                ws.Cells[i + 2, 8].Value = emp.EducationId;
            }
        }

        private static void ExportOrganizations(ExcelPackage p, MSSQLContext db)
        {
            var ws = p.Workbook.Worksheets.Add("Organizations");
            ws.Cells[1, 1].Value = "Id";
            ws.Cells[1, 2].Value = "Name";
            ws.Cells[1, 3].Value = "CountryId";
            var organizations = db.Organizations.ToList();
            for (int i = 0; i < organizations.Count(); i++)
            {
                var org = organizations[i];
                ws.Cells[i + 2, 1].Value = org.Id;
                ws.Cells[i + 2, 2].Value = org.Name;
                ws.Cells[i + 2, 3].Value = org.CountryId;
            }
        }

        private static void ExportWorkPositions(ExcelPackage p, MSSQLContext db)
        {
            var ws = p.Workbook.Worksheets.Add("WorkPositions");
            ws.Cells[1, 1].Value = "Id";
            ws.Cells[1, 2].Value = "Name";
            ws.Cells[1, 3].Value = "Pay";
            var workPositions = db.WorkPositions.ToList();
            for (int i = 0; i < workPositions.Count(); i++)
            {
                var wp = workPositions[i];
                ws.Cells[i + 2, 1].Value = wp.Id;
                ws.Cells[i + 2, 2].Value = wp.Name;
                ws.Cells[i + 2, 3].Value = wp.Pay;
            }
        }
    }
}
