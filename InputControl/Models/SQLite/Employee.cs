﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputControl.Models.SQLite
{
    public class Employee
    {
        [Column("Id")]
        public int Id { get; set; }

        [Column ("first_name")]
        public string FirstName {get; set; }

        [Column("last_name")]
        public string LastName { get; set; }

        [Column("country")]
        public string Country { get; set; }

        [Column("work_position")]
        public string WorkPosition { get; set; }

        [Column("pay")]
        public int Pay { get; set; }

        [Column("organization")]
        public string Organization { get; set; }

        [Column("organization_country")]
        public string OrganizationCountry { get; set; }

        [Column("education")]
        public string Education { get; set; }

        [Column("education_lvl")]
        public string EducationLvl { get; set; }

        [Column("login")]
        public string Login { get; set; }

        [Column("password")]
        public string Password { get; set; }
    }
}
