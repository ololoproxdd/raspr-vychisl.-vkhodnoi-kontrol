﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputControl.Models.MSSQL
{
    public class Employee
    {
        public int Id { get; set; }

        public string Login { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int CountryId { get; set; }

        public int WorkPositionId { get; set; }

        public int OrganizationId { get; set; }

        public int EducationId { get; set; }


        public Employee()
        {

        }
        public Employee(string login, string firstname, string lastname, 
            int countryId, int workpositionId, int organizationId, int educationId)
        {
            Login = login;
            FirstName = firstname;
            LastName = lastname;
            CountryId = countryId;
            WorkPositionId = workpositionId;
            OrganizationId = organizationId;
            EducationId = educationId;
        }
    }
}
