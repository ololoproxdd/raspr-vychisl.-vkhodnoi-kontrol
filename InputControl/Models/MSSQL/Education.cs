﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputControl.Models.MSSQL
{
    public class Education
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Level { get; set; }


        public Education()
        {

        }
        public Education(string name, string level)
        {
            Name = name;
            Level = level;
        }
    }
}
