﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputControl.Models.MSSQL
{
    public class Account
    {
        [Key]
        public string Login { get; set; }

        public string Password { get; set; }

        public Account()
        {

        }
        public Account(string login, string password)
        {
            Login = login;
            Password = password;
        }
    }
}
