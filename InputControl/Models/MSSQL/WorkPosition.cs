﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputControl.Models.MSSQL
{
    public class WorkPosition
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Pay { get; set; }

        public WorkPosition()
        {

        }
        public WorkPosition(string name, int pay)
        {
            Name = name;
            Pay = pay;
        }
    }
}
