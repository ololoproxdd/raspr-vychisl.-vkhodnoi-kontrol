﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputControl.Models.MSSQL
{
    public class Organization
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int CountryId { get; set; }

        public Organization()
        {

        }
        public Organization(string name, int countryId)
        {
            Name = name;
            CountryId = countryId;
        }
    }
}
