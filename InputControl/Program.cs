﻿using InputControl.Context.MSSQL;
using InputControl.Context.SQLite;
using InputControl.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputControl
{
    class Program
    {
        static void Main(string[] args)
        {
            DbService.DeleteMSSQLDb();
            Console.WriteLine("База данных удалена");
            DbService.CreateMSSQLDb();
            Console.WriteLine("База данных создана");
            DbService.Normalization();
            Console.WriteLine("База данных нормализована");
            ExcelService.ExportFromMSSQL();
            Console.WriteLine("База данных экспортирована");
        }
    }
}
