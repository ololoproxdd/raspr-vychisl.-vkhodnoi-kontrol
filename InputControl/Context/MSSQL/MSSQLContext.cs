﻿using InputControl.Models.MSSQL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputControl.Context.MSSQL
{
    public class MSSQLContext : DbContext
    {
        public MSSQLContext() : base("MSSQLConnection")
        {
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Education> Educations { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<WorkPosition> WorkPositions { get; set; }

    }
}
