﻿using InputControl.Models.SQLite;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputControl.Context.SQLite
{
    public class SQLiteContext : DbContext
    {
        public SQLiteContext() : base("SQLiteConnection")
        {
        }

        public DbSet<Employee> Employees { get; set; }
    }
}
